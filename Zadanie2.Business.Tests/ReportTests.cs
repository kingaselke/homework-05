﻿using System;
using DataLayer.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using dane.Repository.Intrface;
using business.Services;
using System.Collections.Generic;
using business.Dtos;
using dane.Repository;

namespace Zadanie2.Business.Tests
{
    [TestClass]
    public class ReportTests
    {

        [TestClass()]
        public class ReportServiceTests
        {
            [TestMethod()]
            public void GenerateReport_ReturnString()
            {

                var course = new Course();
                course.CourseName = "java";
                course.CourseLeader = "ja";
                course.CourseStartDate = new DateTime(2017, 03, 10);
                course.Presence = 100;
                course.ThresholdPoint = 75;
                course.AllStudentList = new List<Person>();

                var courseRepositoryMock = new Mock<IReportRepository>();
                courseRepositoryMock.Setup(x => x.GetCourseByName(It.IsAny<string>())).Returns(course);

               // var reportService = new ReportService(courseRepositoryMock.Object);

               // var result = reportService.GenerateReport("java");

                var cours = new Course();
                cours.CourseName = "java";
                cours.CourseLeader = "ja";
                cours.CourseStartDate = new DateTime(2017, 03, 10);
                cours.Presence = 100;
                cours.ThresholdPoint = 75;

                var report = "nazwa kursu : " + cours.CourseName + "\n" +
                            "prowadzący :" + cours.CourseLeader + "\n" +
                            "data rozpoczęcia kursu :" + cours.CourseStartDate + "\n" +
                            "próg dla obecności :" + cours.Presence + "\n" +
                            "próg dla zadań domowych :" + cours.ThresholdPoint + "\n";

              //  Assert.AreEqual(report, result);
                courseRepositoryMock.Verify(x => x.GetCourseByName("java"), Times.Once);
            }

            [TestMethod()]
            public void Homework_ReturnString()
            {
                var homeworksList = new List<Homework>();
                var h = new Homework();
                h.Pesel = 123;
                h.NumberOfPoints = 10;
                h.MaxNumberOfPoints = 10;
                homeworksList.Add(h);

                var courseRepositoryMock = new Mock<IReportRepository>();
                courseRepositoryMock.Setup(x => x.GetHomeworkFromCourse(It.IsAny<string>())).Returns(homeworksList);

               // var reportService = new ReportService(courseRepositoryMock.Object);

                var course = new Course();
                course.CourseName = "java";
                course.AllStudentList = new List<Person>();
                course.ThresholdPoint = 10;
                var student = new Person();
                student.Pesel = 123;
                student.StudentName = "Kuba";

                course.AllStudentList.Add(student);

                //var result = reportService.Homework(course,);

                var person = new Person();
                person.Pesel = 123;
                person.StudentName = "Kuba";

                var homework = new Homework();
                homework.NumberOfPoints = 10;
                homework.MaxNumberOfPoints = 10;

                var cours = new Course();
                cours.CourseName = "java";
                cours.ThresholdPoint = 10;

                double currentHomeworkPercent =
                    homework.NumberOfPoints / homework.MaxNumberOfPoints * 100;

                string report = person.Pesel + " - " + person.StudentName + "\n";
                report += "Zaliczenie na podstawie prac domowych :" +
                            homework.NumberOfPoints
                                  + "/" + homework.MaxNumberOfPoints
                                  + "(" + currentHomeworkPercent + "%) - "
                                  + (currentHomeworkPercent >= cours.ThresholdPoint ? "ZALICZONE" : "NIEZALICZONE")
                                  + "\n";

               // Assert.AreEqual(report, result);
                courseRepositoryMock.Verify(x => x.GetHomeworkFromCourse("java"), Times.Once);
            }


            [TestMethod()]
            public void Attendance_ReturnString()
            {
                var attendancesList = new List<Attendance>();
                var a = new Attendance();
                a.Pesel = 123;
                a.PresenceNumber = 10;
                a.Days = 10;
                attendancesList.Add(a);

                var courseRepositoryMock = new Mock<IReportRepository>();
                courseRepositoryMock.Setup(x => x.GetAttendanceFromCourse(It.IsAny<string>())).Returns(attendancesList);

               // var reportService = new ReportService(courseRepositoryMock.Object);

                var course = new Course();
                course.CourseName = "java";
                course.AllStudentList = new List<Person>();
                course.Presence = 10;
                var student = new Person();
                student.Pesel = 123;
                student.StudentName = "Kuba";

                course.AllStudentList.Add(student);

              //  var result = reportService.Attendance(course);

                var person = new Person();
                person.Pesel = 123;
                person.StudentName = "Kuba";

                var attendance = new Attendance();
                attendance.PresenceNumber = 10;
                attendance.Days = 10;

                var cours = new Course();
                cours.CourseName = "java";
                cours.ThresholdPoint = 10;

                double currentAttendancePercent =
                                attendance.PresenceNumber / attendance.Days * 100;

                string report = person.Pesel + " - " + person.StudentName + "\n";
                report += "Zaliczenie na podstawie obecności : " +
                                attendance.PresenceNumber
                                      + "/" + attendance.Days
                                      + "(" + currentAttendancePercent + "%) - "
                                      + (currentAttendancePercent >= course.Presence ? "ZALICZONE" : "NIEZALICZONE")
                                      + "\n";

               // Assert.AreEqual(report, result);
                courseRepositoryMock.Verify(x => x.GetAttendanceFromCourse("java"), Times.Once);
            }
        }

        //[TestMethod]

        //public void AddHomework_ReturnsTrue_WhenHomeworkWasFound()
        //{
        //    var mock = new Mock<IHomeworkRepository>();
        //    var homeworkService = new HomeworkService(mock.Object);
        //    mock.Setup(x => x.AddHomework(It.IsAny<Homework>())).Returns(true);

        //    var result = homeworkService.AddHomework("java", new List<HomeworkDTo>());

        //    Assert.AreEqual(result, true);
        //}
        //[TestMethod]

        //public void Result_ReturnsCurrentHomeworkPercent_WhenCalculateResult()
        //{

        //    //Arrange
        //    var homework = new Homework();
        //    homework.NumberOfPoints = 50;
        //    homework.MaxNumberOfPoints = 100;

        //    //Act
        //    double currentHomeworkPercent = (homework.NumberOfPoints / homework.MaxNumberOfPoints) * 100;

        //    //Assert
        //    Assert.AreEqual(currentHomeworkPercent, 50);

        //}
        //[TestMethod]

        //public void Result_ReturnsCurrentAttendancePercent_WhenCalculateResult()
        //{

        //    //Arrange
        //    var attendance = new Attendance();
        //    attendance.PresenceNumber = 5;
        //    attendance.Days = 10;

        //    //Act
        //    double currentHomeworkPercent = (attendance.PresenceNumber / attendance.Days) * 100;

        //    //Assert
        //    Assert.AreEqual(currentHomeworkPercent, 50);

        //}
    }

}
