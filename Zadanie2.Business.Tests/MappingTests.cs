﻿using System;
using System.Collections.Generic;
using System.Configuration;
using business.Dtos;
using business.Mappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using dane.Repository;
using business.Services;
using dane.Repository.Intrface;
using DataLayer.Models;

namespace Business.Tests
{
    [TestClass]
    public class UnitTest1
    {
       

        [TestMethod]
        public void MappingCourse_ProvideValidCOurse_ResiveProperlyMapperCourseDto()
        {
            //Arrange
            var course = new Course();
            course.CourseName = "C";
            course.CourseLeader = "Kuba";
            course.CourseStartDate = DateTime.Parse("1999/01/01");
            course.ThresholdPoint = 50;
            course.Presence = 50;
            course.NumberOfStudents = 2;

            course.AllStudentList = new List<Person>();
            var testData1 = new Person();
            testData1.StudentName = "Kuba";
            testData1.Sex = Person.PersonSex.F;
            testData1.Pesel = 123;
            testData1.StudentSurname = "Bulczi";
            testData1.Birthday = DateTime.Parse("1999/01/01");

            course.AllStudentList.Add(testData1);

            var courseDto = new CourseDTo();
            courseDto.CourseName = "C";
            courseDto.CourseLeader = "Kuba";
            courseDto.CourseStartDate = DateTime.Parse("1999/01/01");
            courseDto.ThresholdPoint = 50;
            courseDto.Presence = 50;
            courseDto.NumberOfStudents = 2;         

            courseDto.AllStudentsList=new List<PersonDto>();
            var testData = new PersonDto();
            testData.StudentName = "Kuba";
            testData.Sex = PersonDto.PersonSex.F;
            testData.Pesel = 123;
            testData.StudentSurname = "Bulczi";
            testData.Birthday = DateTime.Parse("1999/01/01");

            courseDto.AllStudentsList.Add(testData);
            

            //Act
            var coursDto = EntityModelToDto.CourseToCourseDto(course);
            var coursDToList = EntityModelToDto.ListPersonToListPersonDto(course.AllStudentList);

            //Assert
            Assert.AreEqual(courseDto.CourseName, coursDto.CourseName);
            Assert.AreEqual(courseDto.CourseLeader, coursDto.CourseLeader);
            Assert.AreEqual(courseDto.CourseStartDate, coursDto.CourseStartDate);
            Assert.AreEqual(courseDto.ThresholdPoint, coursDto.ThresholdPoint);
            Assert.AreEqual(courseDto.Presence, coursDto.Presence);
            Assert.AreEqual(courseDto.NumberOfStudents, coursDto.NumberOfStudents);
            CollectionAssert.Equals(testData, coursDToList);         
        }

        [TestMethod]
        public void MappingPerson_ProvideValidPerson_ResiveProperlyMapperPersonDto()
        {
            //Arrange
            var person = new Person();
            person.StudentName = "Kuba";
            person.Sex = Person.PersonSex.F;
            person.Pesel = 123;
            person.StudentSurname = "Bulczi";
            person.Birthday = DateTime.Parse("1999/01/01");


            var personDtos = new PersonDto();
            personDtos.StudentName = "Kuba";
            personDtos.Sex = PersonDto.PersonSex.F;
            personDtos.Pesel = 123;
            personDtos.StudentSurname = "Bulczi";
            personDtos.Birthday = DateTime.Parse("1999/01/01");

            //Act
            var personDto = EntityModelToDto.PersonToPersonDto(person);

            //Assert
            Assert.AreEqual(personDtos.StudentName, personDto.StudentName);
            Assert.AreEqual(personDtos.Sex, personDto.Sex);
            Assert.AreEqual(personDtos.Pesel, personDto.Pesel);
            Assert.AreEqual(personDtos.StudentSurname, personDto.StudentSurname);
            Assert.AreEqual(personDtos.Birthday, personDto.Birthday);

        }


        [TestMethod]
        public void MappingCourseDTo_ProvideValidCOurseDTo_ResiveProperlyMapperCourse()
        {
            //Arrange
            var courseDto = new CourseDTo();
            courseDto.CourseName = "C";
            courseDto.CourseLeader = "Kuba";
            courseDto.CourseStartDate = DateTime.Parse("1999/01/01");
            courseDto.ThresholdPoint = 50;
            courseDto.Presence = 50;
            courseDto.NumberOfStudents = 2;

            var course = new Course();
            course.CourseName = "C";
            course.CourseLeader = "Kuba";
            course.CourseStartDate = DateTime.Parse("1999/01/01");
            course.ThresholdPoint = 50;
            course.Presence = 50;
            course.NumberOfStudents = 2;

            //Act
            var cours = DToToEntityModel.CourseDToToCourse(courseDto);

            //Assert
            Assert.AreEqual(course.CourseName, cours.CourseName);
            Assert.AreEqual(course.CourseLeader, cours.CourseLeader);
            Assert.AreEqual(course.CourseStartDate, cours.CourseStartDate);
            Assert.AreEqual(course.ThresholdPoint, cours.ThresholdPoint);
            Assert.AreEqual(course.Presence, cours.Presence);
            Assert.AreEqual(course.NumberOfStudents, cours.NumberOfStudents);
        }

        [TestMethod]
        public void MappingPersonDTo_ProvideValidPersonDTo_ResiveProperlyMapperPerson()
        {
            //Arrange
            var personDtos = new PersonDto();
            personDtos.StudentName = "Kuba";
            personDtos.Sex = PersonDto.PersonSex.F;
            personDtos.Pesel = 123;
            personDtos.StudentSurname = "Bulczi";
            personDtos.Birthday = DateTime.Parse("1999/01/01");


            var person = new Person();
            person.StudentName = "Kuba";
            person.Sex = Person.PersonSex.F;
            person.Pesel = 123;
            person.StudentSurname = "Bulczi";
            person.Birthday = DateTime.Parse("1999/01/01");

            //Act
            var persons = DToToEntityModel.PersonDToToPerson(personDtos);

            //Assert
            Assert.AreEqual(person.StudentName, persons.StudentName);
            Assert.AreEqual(person.Sex, persons.Sex);
            Assert.AreEqual(person.Pesel, persons.Pesel);
            Assert.AreEqual(person.StudentSurname, persons.StudentSurname);
            Assert.AreEqual(person.Birthday, persons.Birthday);

        }

       
    }
}
