﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dane.Repository;
using DataLayer.Models;

namespace dane.Services
{
    public class HomeworkRepositoryService
    {
        public List<Homework> GetHomeworkFromCourse(string courseName)
        {
            var unitOfWork = new RepositoryUnitOfWork();
            var homeworkRepository = unitOfWork.GetRepository<Homework>();
            return homeworkRepository
                .GetAll()
                .Where(x => x.Course.CourseName == courseName)
                .ToList();
        }
    }
}
