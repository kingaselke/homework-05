﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dane.Repository;
using DataLayer.Models;

namespace dane.Services
{
    public class CourseRepositoryService
    {
        public Course GetCourseByName(string courseName)
        {
            var unitOfWork = new RepositoryUnitOfWork();
            var courseRepository = unitOfWork.GetRepository<Course>();
            return courseRepository.GetAll()
                .Include("AllStudentList")
                .SingleOrDefault(c => c.CourseName == courseName);
        }
        public Course GetCourseById(int id)
        {
            var unitOfWork = new RepositoryUnitOfWork();
            var courseRepository = unitOfWork.GetRepository<Course>();
            return courseRepository.GetAll()
                .Include("AllStudentList").SingleOrDefault(c=>c.Id ==id);
        }

        public void AttachPersonToCourse(Course course)
        {
            var unitOfWork = new RepositoryUnitOfWork();
            var personRepository = unitOfWork.GetRepository<Person>();
            foreach (var student in course.AllStudentList)
                personRepository.Attach(student);
        }

        public void ChangeCourseData(Course course)
        {

            var unitOfWork = new RepositoryUnitOfWork();
            var courseRepository = unitOfWork.GetRepository<Course>();


            var cours = courseRepository.GetAll()
                .Include("AllStudentList")
                    .First(p => p.Id == course.Id);

            if (!string.IsNullOrEmpty((course.CourseName)))
                cours.CourseName = course.CourseName;

            if (!string.IsNullOrEmpty((course.CourseLeader)))
                cours.CourseLeader = course.CourseLeader;

            if (course.ThresholdPoint != 0)
                cours.ThresholdPoint = course.ThresholdPoint;

            if (course.Presence != 0)
                cours.Presence = course.Presence;

            bool updateCoursants =
                (cours.AllStudentList.Count() > course.AllStudentList.Count() ? true : false);

            if (updateCoursants)
            {
                cours.AllStudentList = new List<Person>();
                cours.NumberOfStudents = course.AllStudentList.Count();
            }
            unitOfWork.SaveChanges();

            if (updateCoursants) { PushStudentsIntoCours(course.AllStudentList, cours); }
        }




        public void PushStudentsIntoCours(List<Person> people, Course course)
        {
            var unitOfWork = new RepositoryUnitOfWork();
            var personRepository = unitOfWork.GetRepository<Person>();
            var courseRepository = unitOfWork.GetRepository<Course>();
            courseRepository.Attach(course);

            foreach (var student in people)
            {
                personRepository.Attach(student);
                course.AllStudentList.Add(student);
            }

        }
    }

}

