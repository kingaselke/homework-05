﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dane.Repository;
using DataLayer.Models;

namespace dane.Services
{
    public class AttendanceRepositoryService
    {
        public List<Attendance> GetAttendanceFromCourse(string courseName)
        {
            var unitOfWork = new RepositoryUnitOfWork();
            var attendanceRepository = unitOfWork.GetRepository<Attendance>();
            return attendanceRepository
                .GetAll()
                .Where(x => x.Course.CourseName == courseName).ToList();
        }
    }
}
