﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dane.Repository;
using DataLayer.DbContext;
using DataLayer.Models;

namespace dane.Services
{
    public class PersonRepositoryService
    {
        public void ChangeStudentData(Person person, List<Course> courses)
        {
            var unitOfWork = new RepositoryUnitOfWork();
            var personRepository = unitOfWork.GetRepository<Person>();

            var persons = personRepository.GetAll().Include("AllCoursesList").First(p => p.Pesel == person.Pesel);

            if (!string.IsNullOrEmpty((person.StudentName)))
                persons.StudentName = person.StudentName;

            if (!string.IsNullOrEmpty((person.StudentSurname)))
                persons.StudentSurname = person.StudentSurname;

            if (person.Birthday != new DateTime())
                persons.Birthday = person.Birthday;

            bool updateCourses = (persons.AllCoursesList.Count() > courses.Count() ? true : false);
        

            if (updateCourses)
            { PushCoursesIntoPerson(courses,person);}
            unitOfWork.SaveChanges();
        }
    
    private void PushCoursesIntoPerson(List<Course> courses, Person person)
    {
        var unitOfWork = new RepositoryUnitOfWork();
        var courseRepository = unitOfWork.GetRepository<Course>();
            foreach (var cours in courses)
            {
                var sql = courseRepository.db().ExecuteSqlCommand(
                    "DELETE FROM [dbo].[PersonCourses] WHERE " +
                    "[Person_Id]=" + person.Id +
                    " AND [Course_Id]=" + cours.Id);
                var c = courseRepository.GetAll().Include("AllStudentList").First(p => p.Id == cours.Id);
                c.NumberOfStudents--;
                unitOfWork.SaveChanges();
            }
    }
        public Person GetPersonByIdWithCourse(int id)
        {
            var unitOfWork = new RepositoryUnitOfWork();
            var personRepository = unitOfWork.GetRepository<Person>();
            return personRepository.GetAll().Include("AllCoursesList").SingleOrDefault(x => x.Id == id);
        }
    }
}

