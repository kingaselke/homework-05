﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dane.Repository.Interface;

namespace DataLayer.Models
{
    public class Homework : IEntity
    {
        public int Id { get; set; }
        public double MaxNumberOfPoints { get; set; }
        public double NumberOfPoints { get; set; }
        public long Pesel { get; set; }
        public Course Course { get; set; }
        // public List<Homework> Homeworks { get; set; }
    }

}
