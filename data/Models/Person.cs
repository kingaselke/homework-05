﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using dane.Repository.Interface;


namespace DataLayer.Models
{
    public class Person : IEntity
    {
        public int Id { get; set; }
        public enum PersonSex { M, F }
        public long Pesel { get; set; }
        public string StudentName { get; set; }
        public string StudentSurname { get; set; }
        public DateTime Birthday { get; set; }
        public PersonSex Sex { get; set; }
       public List<Course> AllCoursesList { get; set; } 
        //public List<Homework> ListHomework { get; set; }
        //public List<Attendance> ListAttendance { get; set; }
    }
}
