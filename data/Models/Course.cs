﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dane.Repository.Interface;

namespace DataLayer.Models
{
    public class Course : IEntity
    {
        public int Id { get; set; }
        public string CourseName { get; set; }
        public string CourseLeader { get; set; }
        public DateTime CourseStartDate { get; set; }
        public int ThresholdPoint { get; set; }
        public int Presence { get; set; }
        public int NumberOfStudents { get; set; }
        public List<Person> AllStudentList { get; set; }
        public List<Homework> ListHomework { get; set; }
        public List<Attendance> ListAttendance { get; set; }
    }
}
