﻿using System.ComponentModel.DataAnnotations;
using dane.Repository.Interface;
using DataLayer.Models;

namespace DataLayer.Models
{
    public class Attendance : IEntity
    {
        public int Id { get; set; }
        public double PresenceNumber { get; set; }
        public double Days { get; set; }
        public long Pesel { get; set; }
        public Course Course { get; set; }
    }
}