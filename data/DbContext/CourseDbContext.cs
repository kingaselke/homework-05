﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

using DataLayer.Models;
//using System.Configuration;


namespace DataLayer.DbContext
{
    public class CourseDbContext : System.Data.Entity.DbContext
    {

        public CourseDbContext() : base(ConnectionString()) // przekazujemy go do bazy connectionString, (wywolanie konstruktora klasy z której dziedziczy)
        { }

        public DbSet<Attendance> AttendancesDbSet { get; set; }
        public DbSet<Course> CoursesDbSet { get; set; }
        public DbSet<Homework> HomeworksDbSet { get; set; }
        public DbSet<Person> PersonsDbSet { get; set; }
        



        private static string ConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["KingaZblewska"].ConnectionString;
        }
    }
}

