﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dane.Repository.Interface;
using DataLayer.DbContext;

namespace dane.Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class, IEntity
    {
        private CourseDbContext _dbContext;
        private Database _database;

        public DbSet<T> DataSet
        {  
            get { return _dbContext.Set<T>(); }
        }


        public Database db()
        {
            return _database;
        }


        public GenericRepository(CourseDbContext dbContext)
        {
            _dbContext = dbContext;
        }
     
        public void Attach(T entity)
        {
            DataSet.Attach(entity);
        }

        public void Add(T entity)
        {
            DataSet.Add(entity);
        }

        public IQueryable<T> GetAll()
        {
            return DataSet;
        }

       public T Get(int id)
        {
            return GetAll().Single(e => e.Id == id);
        }
        public void Delete(int id)
        {
            DataSet.Remove(Get(id));
        }

        public void Remove(T entity)
        {
           _dbContext.Entry(entity).State = EntityState.Deleted;
            
        }
    }
}

