﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dane.Repository.Interface;
using DataLayer.DbContext;
using DataLayer.Models;

namespace dane.Repository
{
    public class RepositoryUnitOfWork : IRepositoryUnitOfWork
    {
        private CourseDbContext _dbContext;
        private Dictionary<Type, object> _repositories;

        public RepositoryUnitOfWork()
        {
            _dbContext = new CourseDbContext();
            RegisterRepositories();
        }

        public void RegisterRepositories()
        {
            _repositories = new Dictionary<Type, object>();
            Register(typeof(Course), new GenericRepository<Course>(_dbContext));
            Register(typeof(Person), new GenericRepository<Person>(_dbContext));
            Register(typeof(Attendance), new GenericRepository<Attendance>(_dbContext));
            Register(typeof(Homework), new GenericRepository<Homework>(_dbContext));
        }

        public void Register(Type type, object repositry)
        {
            if (_repositories.ContainsKey(type))
            {
                throw new Exception("There is a repository for type " + type.Name + " already defined");
            }

            _repositories.Add(type, repositry);
        }
        public GenericRepository<T> GetRepository<T>() where T : class, IEntity
        {
            return _repositories[typeof(T)] as GenericRepository<T>;
        }

        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }
    }
}
