using System.Data.Entity;
using System.Linq;
using dane.Repository.Interface;

namespace dane.Repository
{
    public interface IGenericRepository<T> where T : class, IEntity
    {
        DbSet<T> DataSet { get; }
        void Attach(T entity);
        void Add(T entity);
        IQueryable<T> GetAll();
        T Get(int id);
        void Delete(int id);
        void Remove(T entity);
    }
}