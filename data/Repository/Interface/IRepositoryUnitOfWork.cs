using System;
using dane.Repository.Interface;

namespace dane.Repository
{
    public interface IRepositoryUnitOfWork
    {
        void RegisterRepositories();
        void Register(Type type, object repositry);
        GenericRepository<T> GetRepository<T>() where T : class, IEntity;
        void SaveChanges();
    }
}