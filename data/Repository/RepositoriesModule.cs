﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Ninject.Modules;

namespace dane.Repository
{
    public class RepositoriesModule : NinjectModule
    {
        public override void Load()
        {
            Bind(typeof(IGenericRepository<>)).To(typeof(GenericRepository<>));
            Bind(typeof(IRepositoryUnitOfWork)).To(typeof(RepositoryUnitOfWork));
        }
    }
}
