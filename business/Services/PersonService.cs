﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using business.Dtos;
using business.Mappers;
using dane.Repository;
using dane.Services;
using DataLayer.Models;
using Ninject;

namespace business.Services
{
    public class PersonService : IPersonService
    {
        private RepositoryUnitOfWork _repositoryUnitOfWork;
        private GenericRepository<Person> _personRepository;

        public PersonService()
        {
            _repositoryUnitOfWork = new RepositoryUnitOfWork();
            _personRepository = _repositoryUnitOfWork.GetRepository<Person>();
        }
        
        public void AddStudent(PersonDto personDTo)
        {
            Person person = DToToEntityModel.PersonDToToPerson(personDTo);
            _personRepository.Add(person);
            _repositoryUnitOfWork.SaveChanges();
        }
        
        public bool CheckIfPersonIsInDbById(int id)
        {
            var personRepositoryService = new PersonRepositoryService();
            var student = personRepositoryService.GetPersonByIdWithCourse(id);

            if (student == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void ChangeStudentData(PersonDto personDto, List<CourseDTo> coursesDto)
        {
            var personRepositoryService = new PersonRepositoryService();
            Person person = DToToEntityModel.PersonDToToPerson(personDto);
            List<Course> courses = DToToEntityModel.ListCourseDtoToListCourse(coursesDto);
            personRepositoryService.ChangeStudentData(person, courses);
            _repositoryUnitOfWork.SaveChanges();

        }

        public void RemovePerson(PersonDto personDto)
        {
            Person person = DToToEntityModel.PersonDToToPerson(personDto);
            _personRepository.Remove(person);
        }

        public static List<CourseDTo> GetAllCoursesByStudentId(int id)
        {
            var personRepositoryService = new PersonRepositoryService();
            var person = personRepositoryService.GetPersonByIdWithCourse(id);
            var coursDto = new List<CourseDTo>();

            foreach (var cours in person.AllCoursesList)
            {
                coursDto.Add(EntityModelToDto.CourseToCourseDto(cours));
            }
            return coursDto;
        }

        public PersonDto GetStudentById(int id)
        {
            var person = _personRepository.Get(id);
            return EntityModelToDto.PersonToPersonDto(person);
        }
    }
}
