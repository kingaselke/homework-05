﻿using business.Dtos;
using business.Mappers;
using dane.Repository;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dane.Services;
using Ninject;


namespace business.Services
{
    public class CourseService : ICourseService
    {
        private RepositoryUnitOfWork _repositoryUnitOfWork;
        private GenericRepository<Course> _courseRepository;
        private GenericRepository<Person> _personRepository;

        public CourseService()
        {
            _repositoryUnitOfWork = new RepositoryUnitOfWork();
            _courseRepository = _repositoryUnitOfWork.GetRepository<Course>();
            _personRepository = _repositoryUnitOfWork.GetRepository<Person>();

        }
        
        public void CourseWithStudents(CourseDTo courseDto, List<int> peopleList)
        {
            _courseRepository = _repositoryUnitOfWork.GetRepository<Course>();
            _personRepository = _repositoryUnitOfWork.GetRepository<Person>();
            var course = DToToEntityModel.CourseDToToCourse(courseDto);
            var student = new List<Person>();
            foreach (var id in peopleList)
            {
                var person = _personRepository.Get(id);
                student.Add(person);

            }
            course.AllStudentList = student;
            _courseRepository.Add(course);
            _repositoryUnitOfWork.SaveChanges();
        }

        public bool CheckIfCourseIsInDb(int id)
        {
            var courseRepositoryService = new CourseRepositoryService();
            var course = courseRepositoryService.GetCourseById(id);

            if (course == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public List<PersonDto> GetAllStudent(int id)
        {
            var courseRepository = new CourseRepositoryService();

            var course = courseRepository.GetCourseById(id);
            var personDto = new List<PersonDto>();

            foreach (var person in course.AllStudentList)
            {
                personDto.Add(EntityModelToDto.PersonToPersonDto(person));
            }

            return personDto;
        }

        public CourseDTo GetCourseById(int id)
        {
            return EntityModelToDto.CourseToCourseDto(_courseRepository.Get(id));
        }

        public List<PersonDto> GetAllStudentByCourseId(int id)
        {
            var courseRepository = new CourseRepositoryService();
            var course = courseRepository.GetCourseById(id);
            var personDto = new List<PersonDto>();

            foreach (var person in course.AllStudentList)
                personDto.Add(EntityModelToDto.PersonToPersonDto(person));

            return personDto;
        }

        public void ChangeCourseDate(CourseDTo courseDto)
        {
            var course = DToToEntityModel.CourseDToToCourse(courseDto);
            var courseRepositoryService = new CourseRepositoryService();
            courseRepositoryService.ChangeCourseData(course);
            _repositoryUnitOfWork.SaveChanges();
        }
    }
}
