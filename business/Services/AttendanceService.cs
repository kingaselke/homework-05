﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using business.Dtos;
using business.Mappers;
using dane.Repository;
using DataLayer.Models;
using Ninject;

namespace business.Services
{
    public class AttendanceService : IAttendanceService
    {
        private RepositoryUnitOfWork _repositoryUnitOfWork;
        private GenericRepository<Attendance> _attendanceRepository;
        private GenericRepository<Course> _courseRepository;

        public AttendanceService()
        {
            _repositoryUnitOfWork = new RepositoryUnitOfWork();
            _attendanceRepository = _repositoryUnitOfWork.GetRepository<Attendance>();
            _courseRepository = _repositoryUnitOfWork.GetRepository<Course>();
        }

        public void AddAttenadance(int id, List<AttendanceDTo> attendanceDtos)
        {
            var course = _courseRepository.Get(id);
            var attendances = attendanceDtos.Select(hw => DToToEntityModel.AttendanceDtoToAttendance(hw));

            foreach (var attendance in attendances)
            {
                attendance.Course = course;
                _attendanceRepository.Add(attendance);
                _repositoryUnitOfWork.SaveChanges();
            }
        }
    }
}
