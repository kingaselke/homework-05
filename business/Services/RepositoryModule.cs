﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dane.Repository;

using Ninject.Modules;

namespace business.Services
{
   public class RepositoryModule:NinjectModule
    {
        public override void Load()
        {
            Bind(typeof(IGenericRepository<>)).To(typeof(GenericRepository<>));
            Bind(typeof(IRepositoryUnitOfWork)).To(typeof(RepositoryUnitOfWork));
        }
    }
}
