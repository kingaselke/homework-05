﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using business.Dtos;
using business.Mappers;
//using business.Services.proxy;
using dane.Repository;
using DataLayer.Models;

using dane.Services;
using Ninject;
using Zadanie2;

namespace business.Services
{
    public class ReportService : IReportService
    {
        private GenericRepository<Course> _courseRepository;
       private GenericRepository<Person> _personRepository;

        public ReportService()
        {
            var courseUnitOfWork = new RepositoryUnitOfWork();
            _courseRepository = courseUnitOfWork.GetRepository<Course>();
          _personRepository = courseUnitOfWork.GetRepository<Person>();
        }

        public string PrintRaport(RaportObject raportObject)
        {
            var course = _courseRepository.Get(raportObject.Id);
            // var id = ConsoleReadHelper.GetCourse();
            var report = "nazwa kursu : " + course.CourseName + "\n" + "prowadzący :" + course.CourseLeader + "\n" +
                         "data rozpoczęcia kursu :" + course.CourseStartDate + "\n" +
                         "próg dla obecności :" + course.Presence + "\n" + "próg dla zadań domowych :" +
                         course.ThresholdPoint + "\n" + Homework(course, raportObject) + Attendance(course, raportObject);
            return report;
        }

        public RaportObject GenerateReport(int id)
        {
            var courseRepositoryService = new CourseRepositoryService();
            var attendanceRepoService = new AttendanceRepositoryService();
            var homeworkRepoService = new HomeworkRepositoryService();
            var raportObjects = new RaportObject();

            var course = courseRepositoryService.GetCourseById(id);
           var listOfHomeworks = homeworkRepoService.GetHomeworkFromCourse(course.CourseName);
            var listOfAttendances = attendanceRepoService.GetAttendanceFromCourse(course.CourseName);
            
            raportObjects.Id = course.Id;
            raportObjects.CourseName = course.CourseName;
            raportObjects.StaretDate = course.CourseStartDate;
            raportObjects.LeaderName = course.CourseLeader;
            raportObjects.ThresholdPoint = course.ThresholdPoint;
            raportObjects.Presence = course.Presence;
            raportObjects.AllPersons = new List<PersonRaport>();
            
            foreach (var person1 in course.AllStudentList)
            {
                var personRaport = new PersonRaport();

                personRaport.StudentName = person1.StudentName;
                personRaport.StudentSurname = person1.StudentSurname;
                personRaport.Pesel = person1.Pesel;
                personRaport.AllAttendances = new List<AttendancePointRaport>();
                personRaport.AllHomeworks = new List<HomeworkPointRaport>();
                foreach (var homework in listOfHomeworks)
                {
                    if (homework.Pesel == personRaport.Pesel)
                    {

                        var homeworkPointRaport = new HomeworkPointRaport();
                        homeworkPointRaport.MaxNumerOfPoints = homework.MaxNumberOfPoints;
                        homeworkPointRaport.NumberOfPoints = homework.NumberOfPoints;
                        personRaport.AllHomeworks.Add(homeworkPointRaport);
                    }
                }

                foreach (var attendance in listOfAttendances)
                {
                    if (attendance.Pesel == personRaport.Pesel)
                    {
                        var attendancePointRaport = new AttendancePointRaport();
                        attendancePointRaport.Days = attendance.Days;
                        attendancePointRaport.PresenceNumber = attendance.PresenceNumber;
                        personRaport.AllAttendances.Add(attendancePointRaport);
                    }
                }
                raportObjects.AllPersons.Add(personRaport);
            }

            return raportObjects;
        }

        public string Homework(Course course, RaportObject raportObject)
        {
            var homeworkRepoService = new HomeworkRepositoryService();
            var listOfHomeworks = homeworkRepoService.GetHomeworkFromCourse(course.CourseName);
            var report = "";
            var studentList = _personRepository.GetAll();
            var studentsForCourse = studentList;//.Where(p => p.Id == course.Id);
            foreach (var person in studentsForCourse)
            {
                report += person.Pesel + " - " + person.StudentName + "\n";
                foreach (var homework in listOfHomeworks)
                {
                    if (homework.Pesel == person.Pesel)
                    {
                        double currentHomeworkPercent =
                            homework.NumberOfPoints / homework.MaxNumberOfPoints * 100;
                        report += "Zaliczenie na podstawie prac domowych :" +
                            homework.NumberOfPoints
                                  + "/" + homework.MaxNumberOfPoints
                                  + "(" + currentHomeworkPercent + "%) - "
                                  + (currentHomeworkPercent >= course.ThresholdPoint ? "ZALICZONE" : "NIEZALICZONE")
                                  + "\n";
                    }
                }
            }
            return report;
        }

        public string Attendance(Course course, RaportObject raportObject)
        {
            var attendanceRepoService = new AttendanceRepositoryService();
            var listOfAttendances = attendanceRepoService.GetAttendanceFromCourse(course.CourseName);
            var report = "";
            var studentList = _personRepository.GetAll();
            var studentsForCourse = studentList;//.Where(p => p.Id == course.Id);
            foreach (var person in studentsForCourse)
            {
                report += person.Pesel + " - " + person.StudentName + "\n";
                foreach (var attendance in listOfAttendances)
                {
                    if (attendance.Pesel == person.Pesel)
                    {
                        double currentAttendancePercent =
                             attendance.PresenceNumber / attendance.Days * 100;
                        report += "Zaliczenie na podstawie obecności : " +
                            attendance.PresenceNumber
                                  + "/" + attendance.Days
                                  + "(" + currentAttendancePercent + "%) - "
                                  + (currentAttendancePercent >= course.Presence ? "ZALICZONE" : "NIEZALICZONE")
                                  + "\n";
                    }
                }
            }
            return report;
        }

    }
}