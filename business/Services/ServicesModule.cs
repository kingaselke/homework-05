﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Modules;

namespace business.Services
{
   public class ServicesModule :NinjectModule
    {
        public override void Load()
        {
           Bind<IAttendanceService>().To<AttendanceService>();
            Bind<ICourseService>().To<CourseService>();
            Bind<IHomeworkService>().To<HomeworkService>();
            Bind<IPersonService>().To<PersonService>();
            Bind<IReportService>().To<ReportService>();
        }

    }
}
