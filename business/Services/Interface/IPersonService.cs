using System.Collections.Generic;
using business.Dtos;

namespace business.Services
{
    public interface IPersonService
    {
        void AddStudent(PersonDto personDTo);
        bool CheckIfPersonIsInDbById(int id);
        void ChangeStudentData(PersonDto personDto, List<CourseDTo> coursesDto);
        void RemovePerson(PersonDto personDto);
        PersonDto GetStudentById(int id);
    }
}