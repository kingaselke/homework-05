﻿using System.Collections.Generic;
using business.Dtos;

namespace business.Services
{
    public interface ICourseService
    {
        
        bool CheckIfCourseIsInDb(int id);
        CourseDTo GetCourseById(int id);
        void ChangeCourseDate(CourseDTo courseDto);
        void CourseWithStudents(CourseDTo courseDto, List<int> peopleList);
    }
}