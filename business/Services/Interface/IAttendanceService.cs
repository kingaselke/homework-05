using System.Collections.Generic;
using business.Dtos;

namespace business.Services
{
    public interface IAttendanceService
    {
        void AddAttenadance(int id, List<AttendanceDTo> attendanceDtos);
    }
}