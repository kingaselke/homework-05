using System.Collections.Generic;
using business.Dtos;

namespace business.Services
{
    public interface IHomeworkService
    {
        void AddHomework(int id, List<HomeworkDTo> homeworkDtos);
       
    }
}