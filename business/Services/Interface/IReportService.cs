using DataLayer.Models;
using Zadanie2;

namespace business.Services
{
    public interface IReportService
    {
        RaportObject GenerateReport(int id);
        string Homework(Course course, RaportObject raportObject);
        string Attendance(Course course, RaportObject raportObject);
        string PrintRaport(RaportObject raportObject);
    }
}