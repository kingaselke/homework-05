﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using business.Dtos;
using business.Mappers;
using dane.Repository;

using dane.Services;
using DataLayer.DbContext;
using DataLayer.Models;
using Ninject;

namespace business.Services
{
    public class HomeworkService : IHomeworkService
    {
        private RepositoryUnitOfWork _repositoryUnitOfWork;
        private GenericRepository<Course> _courseRepository;
        private GenericRepository<Homework> _homeworkRepository;

        public HomeworkService()
        {
            _repositoryUnitOfWork = new RepositoryUnitOfWork();
            _courseRepository = _repositoryUnitOfWork.GetRepository<Course>();
            _homeworkRepository = _repositoryUnitOfWork.GetRepository<Homework>();
        }

        public void AddHomework(int id, List<HomeworkDTo> homeworkDtos)
        {
            var course = _courseRepository.Get(id);
            var homeworks = homeworkDtos.Select(hw => DToToEntityModel.HomeworkDtoToHomework(hw)).ToList();

            foreach (var homework in homeworks)
            {
                homework.Course = course;
                _homeworkRepository.Add(homework);
                _repositoryUnitOfWork.SaveChanges();


            }
        }
    }
}
