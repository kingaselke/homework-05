﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace business.Dtos
{
    public class AttendanceDTo
    {
        public int Id;
        public double PresenceNumber;
        public double Days;
        public long Pesel;
    }
}
