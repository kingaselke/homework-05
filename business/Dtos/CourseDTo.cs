﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace business.Dtos
{
    public class CourseDTo
    {
        public int Id;
        public string CourseName;
        public string CourseLeader;
        public DateTime CourseStartDate;
        public int ThresholdPoint;
        public int Presence;
        public int NumberOfStudents;
        public List <PersonDto> AllStudentsList = new List<PersonDto>();
    }
}
