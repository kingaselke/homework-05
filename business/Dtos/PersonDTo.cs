﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace business.Dtos
{
    public class PersonDto
    {
        public int Id;
        public enum PersonSex { M, F }
        public long Pesel;
        public string StudentName;
        public string StudentSurname;
        public DateTime Birthday;
        public PersonSex Sex { get; set; }
        public static List<CourseDTo> ListOfCourses = new List<CourseDTo>();
        public List< HomeworkDTo> ListHomework = new List<HomeworkDTo>();
        public List< AttendanceDTo> ListAttendance= new List<AttendanceDTo>();
    }
}
