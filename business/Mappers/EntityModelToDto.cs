﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using business.Dtos;

using DataLayer.Models;

namespace business.Mappers
{
    public class EntityModelToDto
    {


        public static CourseDTo CourseToCourseDto(Course course)
        {
            var courseToDto = new CourseDTo();
            courseToDto.Id = course.Id;
            courseToDto.CourseName = course.CourseName;
            courseToDto.CourseLeader = course.CourseLeader;
            courseToDto.CourseStartDate = course.CourseStartDate;
            courseToDto.ThresholdPoint = course.ThresholdPoint;
            courseToDto.Presence = course.Presence;
            courseToDto.NumberOfStudents = course.NumberOfStudents;
            courseToDto.AllStudentsList = ListPersonToListPersonDto(course.AllStudentList);
            return courseToDto;
        }


        public static PersonDto PersonToPersonDto(Person person)
        {
            var personToDto = new PersonDto();
            personToDto.Id = person.Id;
            personToDto.StudentName = person.StudentName;
            personToDto.StudentSurname = person.StudentSurname;
            personToDto.Pesel = person.Pesel;
            personToDto.Birthday = person.Birthday;
            personToDto.Sex = (PersonDto.PersonSex)person.Sex;

            return personToDto;
        }


        public static List<CourseDTo> ListCourseToListCourseDto(List<Course> listCourse)
        {
            List<CourseDTo> list = new List<CourseDTo>();
            if (listCourse == null)
            {
                return list;
            }
            foreach (var student in listCourse)
            {
                list.Add(CourseToCourseDto(student));
            }
            return list;
        }


        public static List<PersonDto> ListPersonToListPersonDto(List<Person> listPerson)
        {
            List<PersonDto> list = new List<PersonDto>();
            if (listPerson == null)
            {
                return list;
            }

            foreach (var student in listPerson)
            {
                list.Add(PersonToPersonDto(student));
            }
            return list;
        }

        public static HomeworkDTo HomeworkTohomeworkDto(Homework homework)
        {
            var homeworkToDto = new HomeworkDTo();
            homeworkToDto.Id = homework.Id;
            homeworkToDto.MaxNumberOfPoints = homework.MaxNumberOfPoints;
            homeworkToDto.NumberOfPoints = homework.NumberOfPoints;
            homeworkToDto.Pesel = homework.Pesel;
            return homeworkToDto;
        }

        public static AttendanceDTo AttendanceToattendanceDTo(Attendance attendance)
        {
            var attendanceDTo = new AttendanceDTo();
            attendanceDTo.Id = attendance.Id;
            attendanceDTo.PresenceNumber = attendance.PresenceNumber;
            attendanceDTo.Days = attendance.Days;
            attendanceDTo.Pesel = attendance.Pesel;
            return attendanceDTo;
        }
    }
}
