﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using business.Dtos;

using DataLayer.Models;

namespace business.Mappers
{
    public class DToToEntityModel
    {
        public static Person PersonDToToPerson(PersonDto personDto)
        {
            var person = new Person();
            person.Id = personDto.Id;
            person.StudentName = personDto.StudentName;
            person.StudentSurname = personDto.StudentSurname;
            person.Pesel = personDto.Pesel;
            person.Birthday = personDto.Birthday;
            person.Sex = (Person.PersonSex)personDto.Sex;
            return person;
        }

        public static Course CourseDToToCourse(CourseDTo courseDTo)
        {
            var course = new Course();
            course.Id = courseDTo.Id;
            course.CourseName = courseDTo.CourseName;
            course.CourseLeader = courseDTo.CourseLeader;
            course.CourseStartDate = courseDTo.CourseStartDate;
            course.ThresholdPoint = courseDTo.ThresholdPoint;
            course.Presence = courseDTo.Presence;
            course.NumberOfStudents = courseDTo.NumberOfStudents;
            course.AllStudentList = ListPersonDtoToListPerson(courseDTo.AllStudentsList);
            return course;
        }

        public static List<Course> ListCourseDtoToListCourse(List<CourseDTo> listCourseDto)
        {
            List<Course> list = new List<Course>();
            if (listCourseDto == null)
            {
                return list;
            }
            foreach (var student in listCourseDto)
            {
                list.Add(CourseDToToCourse(student));
            }
            return list;
        }

        public static List<Person> ListPersonDtoToListPerson(List<PersonDto> listPersonDto)
        {
            List<Person> list = new List<Person>();
            if (listPersonDto == null)
            {
                return list;
            }
            foreach (var student in listPersonDto)
            {
                list.Add(PersonDToToPerson(student));
            }
            return list;
        }

        public static Homework HomeworkDtoToHomework(HomeworkDTo homeworkDTo)
        {
            var homework = new Homework();
            homework.Id = homeworkDTo.Id;
            homework.MaxNumberOfPoints = homeworkDTo.MaxNumberOfPoints;
            homework.NumberOfPoints = homeworkDTo.NumberOfPoints;
            homework.Pesel = homeworkDTo.Pesel;
            return homework;
        }
        
        public static Attendance AttendanceDtoToAttendance(AttendanceDTo attendanceDTo)
        {
            var attendance = new Attendance();
            attendance.Id = attendanceDTo.Id;
            attendance.PresenceNumber = attendanceDTo.PresenceNumber;
            attendance.Days = attendanceDTo.Days;
            attendance.Pesel = attendanceDTo.Pesel;
            return attendance;
        }


    }
}
