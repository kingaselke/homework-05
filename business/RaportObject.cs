﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Models;

namespace Zadanie2
{
    public class RaportObject
    {
        public int Id;
        public string CourseName;
        public DateTime StaretDate;
        public string LeaderName;
        public double ThresholdPoint;
        public double Presence;



        public List<PersonRaport> AllPersons = new List<PersonRaport>();


    }

    public class PersonRaport
    {

        public long Pesel;
        public string StudentName;
        public string StudentSurname;

        public List<HomeworkPointRaport> AllHomeworks = new List<HomeworkPointRaport>();
        public List<AttendancePointRaport> AllAttendances = new List<AttendancePointRaport>();

    }

    public class HomeworkPointRaport
    {
        public double NumberOfPoints;
        public double MaxNumerOfPoints;

    }
    public class AttendancePointRaport
    {
    
        public double PresenceNumber;
        public double Days;
    }
}
