﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using business;
using business.Services;
using dane.Repository;
using Ninject;

namespace Zadanie2
{
    class Program
    {

        static void Main(string[] args)

        {
            IKernel kernel = new StandardKernel(new ServicesModule(), new RepositoriesModule(), new ProgramLoopModule());
            kernel.Get<ProgramLoop>().Execute();
            
        }
    }
}
