﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using business.Services;
using Ninject.Modules;

namespace Zadanie2
{
   public class ProgramLoopModule: NinjectModule
    {
        public override void Load()
        {
            Bind<IProgramLoop>().To<ProgramLoop>();
        }

    }
}
