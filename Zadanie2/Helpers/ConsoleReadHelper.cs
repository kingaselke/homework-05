﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using business.Services;
using dane.Repository;
using Ninject;

namespace Zadanie2.Helpers
{
    public class ConsoleReadHelper
    {
        public static long GetLong(string message)
        {
            while (true)
            {
                Console.WriteLine(message);
                long value;
                if (long.TryParse(Console.ReadLine(), out value))
                    return value;
            }
        }
        public static int GetInt(string message)
        {
            while (true)
            {
                Console.WriteLine(message);
                int value;
                if (int.TryParse(Console.ReadLine(), out value))
                    return value;
            }
        }

        public static int GetInt2(string val, string message)
        {
            int value;
            if (int.TryParse(val, out value))
                return value;

            while (true)
            {
                Console.WriteLine(message);

                if (int.TryParse(Console.ReadLine(), out value))
                    return value;
            }
        }

        public static int GetIntOrNull(string message)
        {
            while (true)
            {
                Console.WriteLine(message);
                int value;
                string input = Console.ReadLine();
                if (int.TryParse(input, out value))
                    return value;
                else if (string.IsNullOrEmpty(input))
                    return 0;
            }
        }

        public static DateTime GetDate(string message)
        {
            while (true)
            {
                Console.WriteLine(message);
                DateTime value;
                if (DateTime.TryParse(Console.ReadLine(), out value))
                    return value;
            }
        }

        public static DateTime GetDateOrNull(string message)
        {
            while (true)
            {
                Console.WriteLine(message);
                string input = Console.ReadLine();
                DateTime value;
                if (DateTime.TryParse(input, out value))
                    return value;
                else if (string.IsNullOrEmpty(input))
                    return new DateTime();
            }
        }

        public static string GetCommandType(string message)
        {
            Console.Write(message + "\n" +
                            "1. Dodaj studenta.\n" +
                            "2. Dodaj kurs.\n" +
                            "3. Dodaj zadanie domowe.\n" +
                            "4. Dodaj obecność.\n" +
                            "5. Wydrukuj raport.\n" +
                            "6. Zmień dane kursanta.\n" +
                            "7. Zmień dane kursu.\n" +
                            "8. Odczytaj dane z pliku.\n" +
                            "9. Wyjście.\n");

            string command = Console.ReadLine();
            return command;
        }

        public static int GetCourse()
        {
            Console.WriteLine("podaj id kursu");
           var courseService = new CourseService();
            int id;
            do
            {
                id = int.Parse(Console.ReadLine());
                if (!courseService.CheckIfCourseIsInDb(id))
                {
                    Console.WriteLine("Brak kursu o podanej nazwie. Wprowadź inną nazwę");
                }

            } while (!courseService.CheckIfCourseIsInDb(id));
            return id;
        }

    }
}

