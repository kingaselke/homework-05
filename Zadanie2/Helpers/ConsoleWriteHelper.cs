﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using business.Dtos;

namespace Zadanie2.Helpers
{
    class ConsoleWriteHelper
    {
        public static void PrintCourseDto(CourseDTo course)
        {
            Console.WriteLine(course.CourseName);
            course.AllStudentsList.ForEach(b => PrintStudentDto(b));
        }

        public static void PrintStudentDto(PersonDto person)
        {
            Console.WriteLine(person.StudentName + " " + person.StudentSurname + " ");
            person.ListHomework.ForEach((b => PrintHomeworkDto(b)));
            person.ListAttendance.ForEach((b => PrintAttendanceDto(b)));

        }

        public static void PrintHomeworkDto(HomeworkDTo homework)
        {
            Console.WriteLine(homework.NumberOfPoints);
            Console.WriteLine(homework.MaxNumberOfPoints);
        }
        public static void PrintAttendanceDto(AttendanceDTo attendance)
        {
            Console.WriteLine(attendance.PresenceNumber);
            Console.WriteLine(attendance.Days);
        }
    }
}