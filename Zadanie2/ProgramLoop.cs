﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using business.Dtos;
using business.Services;
using Newtonsoft.Json;
using Zadanie2.Helpers;

namespace Zadanie2
{
    public class ProgramLoop : IProgramLoop
    {

        private readonly ICourseService _courseService;
        private readonly IPersonService _personService;
        private readonly IReportService _reportService;
        private readonly IAttendanceService _attendanceService;
        private readonly IHomeworkService _homeworkService;

        public ProgramLoop(ICourseService courseService, IHomeworkService homeworkService, IReportService reportService, IPersonService personService, IAttendanceService attendanceService)
        {
            _courseService = courseService;
            _personService = personService;
            _reportService = reportService;
            _attendanceService = attendanceService;
            _homeworkService = homeworkService;

        }

        public delegate void RaportPrintedEventHandler(object sender, RaportPrintedEventArgs args);
        public event RaportPrintedEventHandler RaportPrinted;

        private void OnRaportPrinted(int id)
        {
            if (RaportPrinted == null)
                return;
            var eventArgs = new RaportPrintedEventArgs();
            eventArgs.raportObject = _reportService.GenerateReport(id);
            RaportPrinted(this, eventArgs);
        }

        public CommandDispacher GetCommandDispacher()
        {
            CommandDispacher commandDispacher = new CommandDispacher();
            commandDispacher.TrafsferDictionary().Add("1", AddPerson);
            commandDispacher.TrafsferDictionary().Add("2", AddCourse);
            commandDispacher.TrafsferDictionary().Add("3", AddHomework);
            commandDispacher.TrafsferDictionary().Add("4", AddAttendance);
            commandDispacher.TrafsferDictionary().Add("5", PrintRaport);
            commandDispacher.TrafsferDictionary().Add("6", ChangeStudentData);
            commandDispacher.TrafsferDictionary().Add("7", ChangeCourseData);
            commandDispacher.TrafsferDictionary().Add("8", ReadFile);
            return commandDispacher;
        }
        public void Execute()
        {
            var exit = false;
            while (!exit)
            {
                var command = ConsoleReadHelper.GetCommandType("Podaj numer polecenia:");
                if (GetCommandDispacher().TrafsferDictionary().ContainsKey(command))
                {
                    GetCommandDispacher().TrafsferDictionary()[command].Invoke();
                }
                else if (command == "9")
                {
                    exit = true;
                }
            }
        }

        public void PrintRaport()
        {
            bool work = true;
            while (work)
            {
                Console.WriteLine("Podaj id kursu:");
                int id = int.Parse(Console.ReadLine());
                if (_courseService.CheckIfCourseIsInDb(id))
                {
                    Console.WriteLine(_reportService.PrintRaport(_reportService.GenerateReport(id)));

                    RaportPrinted += SaveFile;

                    OnRaportPrinted(id);
                    work = false;
                }
                else
                {
                    Console.WriteLine("Nie ma takiego kursu w bazie.");
                }
            }
        }

        private void ReadFile()

        {
            Console.WriteLine("Podaj ścieżkę do pliku :");
            var filePath = Console.ReadLine();
            var content = File.ReadAllText(filePath);
            var raportObjectFromJson = JsonConvert.DeserializeObject<RaportObject>(content);

            Console.WriteLine("Nazwa kursu: " + raportObjectFromJson.CourseName +
                              "\nDane prowadzącego: " + raportObjectFromJson.LeaderName +
                              "\n");


            int numberOfHPeople = 0;
            int k=0;
            int numberOfAPeople = 0;
            int l = 0;
            
            double sumOfPercentagePointAllH = 0;
            double sumOfPercentagePointAllA = 0;

            
            double average=0;
            foreach (var person in raportObjectFromJson.AllPersons)
            {
                double sumOfPercentagePoint = 0;
                int i = 0;
                double singleHomeworkAverge = 0;
                foreach (var homework in person.AllHomeworks)
                {

                    sumOfPercentagePoint = sumOfPercentagePoint + homework.NumberOfPoints / homework.MaxNumerOfPoints * 100;
                    sumOfPercentagePointAllH= sumOfPercentagePointAllH + homework.NumberOfPoints / homework.MaxNumerOfPoints * 100;
                    i++;
                    k++;

                }

                singleHomeworkAverge = sumOfPercentagePoint / i;
                average = sumOfPercentagePointAllH / k;
               
                if (singleHomeworkAverge >= raportObjectFromJson.ThresholdPoint)
                {
                    numberOfHPeople++;
                }
            }
            Console.WriteLine("Średni procent z prac domowych : " + average);
            Console.WriteLine("Kurs zaliczylo " + numberOfHPeople + " ludzi");

            foreach (var person in raportObjectFromJson.AllPersons)
            {
                double sumOfPercentagePoint = 0;
                int i = 0;
                double singleAttendanceAverge = 0;
                foreach (var attendance in person.AllAttendances)
                {
                    sumOfPercentagePoint = sumOfPercentagePoint + attendance.PresenceNumber / attendance.Days * 100;
                    sumOfPercentagePointAllA = sumOfPercentagePointAllA + attendance.PresenceNumber / attendance.Days * 100;
                    i++;
                    l++;
                }
                singleAttendanceAverge = sumOfPercentagePoint / i;
                average = sumOfPercentagePointAllA / l;


                if (singleAttendanceAverge >= raportObjectFromJson.Presence)
                {
                    numberOfAPeople++;
                }
            }
            Console.WriteLine("Średni procent z obecności : " + average);
            Console.WriteLine("Kurs zaliczylo " + numberOfAPeople + " ludzi");

        }

        private void SaveFile(object sender, RaportPrintedEventArgs args)
        {
            Console.WriteLine("Czy chcesz zapisać dane do pliku (yes/no) ?");
            while (true)
            {
                var answer = Console.ReadLine();
                if (answer == "yes")
                {
                    Console.WriteLine("Podaj nazwę pliku:");
                    var fileName = Console.ReadLine();
                    string json = JsonConvert.SerializeObject(args.raportObject);
                    File.WriteAllText(fileName, json);

                    break;
                }
                else if (answer == "no")
                {
                    break;
                }
                else { Console.WriteLine("Czy chcesz zapisać dane do pliku (yes/no) ?"); }
            }
        }

        public void ChangeStudentData()
        {
            int id;

            while (true)
            {
                id = ConsoleReadHelper.GetInt("Podaj id osoby której chcesz zmienić dane");
                if (_personService.CheckIfPersonIsInDbById(id)) { break; }
                else { Console.WriteLine("Podany pesel nie występuje w bazie!"); }
            }

            var personDto = _personService.GetStudentById(id);

            Console.WriteLine("Podaj nowe imię");
            string name = Console.ReadLine();
            personDto.StudentName = name;
            Console.WriteLine("Podaj nowe nazwisko");
            string surname = Console.ReadLine();
            personDto.StudentSurname = surname;
            personDto.Birthday = ConsoleReadHelper.GetDateOrNull("Podaj nową datę urodzenia");

            List<CourseDTo> studentAllCourses = PersonService.GetAllCoursesByStudentId(id);
            List<CourseDTo> coursesToEdit = new List<CourseDTo>();

            while (true)
            {
                Console.WriteLine("Podaj Id kursu z którego chcesz wypisać osobę lub wpisz Wyjscie aby wyjść");
                string msg = Console.ReadLine();
                if (msg.ToLower() == "wyjscie")
                {
                    break;
                }
                int idCourse = ConsoleReadHelper.GetInt2(msg, "Podaj Id kursu z którego chcesz wypisać osobę");
                var selectedCours = studentAllCourses.FirstOrDefault(s => s.Id == idCourse);
                if (selectedCours != null)
                {
                    studentAllCourses.Remove(selectedCours);
                    var courseService = new CourseService();
                    selectedCours.AllStudentsList = courseService.GetAllStudentByCourseId(selectedCours.Id);
                    selectedCours.AllStudentsList.Remove(
                        selectedCours.AllStudentsList.First(k => k.Id == idCourse));
                    coursesToEdit.Add(selectedCours);
                }
                else
                {
                    Console.WriteLine("Student nie uczęszcza na podany kurs.");
                }
            }
            _personService.ChangeStudentData(personDto, coursesToEdit);
        }

        public void ChangeCourseData()
        {
            var courseDto = new CourseDTo();

            courseDto.Id = ConsoleReadHelper.GetInt("Podaj Id kursu którego chcesz zmienić dane:");
            Console.WriteLine("Podaj nową nazwę kursu");
            courseDto.CourseName = Console.ReadLine();
            Console.WriteLine("Podaj nowe dane prowadzącego");
            courseDto.CourseLeader = Console.ReadLine();
            courseDto.ThresholdPoint = ConsoleReadHelper.GetIntOrNull("Podaj nowy próg punktowy dla zadań domowych");
            courseDto.Presence = ConsoleReadHelper.GetIntOrNull("Podaj nowy próg punktowy dla obecnosci");
            var courseService = new CourseService();
            courseDto.AllStudentsList = courseService.GetAllStudentByCourseId(courseDto.Id);

            while (true)
            {
                Console.WriteLine("Podaj pesel osoby którą chcesz" +
                    " wypisać z kursu lub wpisz Wyjscie aby wyjść");
                string msg = Console.ReadLine();
                if (msg.ToLower() == "wyjscie")
                {
                    break;
                }
                int id = ConsoleReadHelper.GetInt2(msg, "Podaj id osoby " +
                    "któą chcesz wypiasc z kursu");

                if (_personService.CheckIfPersonIsInDbById(id))
                {
                    var person = courseDto.AllStudentsList.SingleOrDefault(x => x.Id == id);
                    if (person != null)
                        courseDto.AllStudentsList.Remove(person);
                    else
                        Console.WriteLine("Wybrany student nie uczęszcza na podany kurs.");
                }
                else
                {
                    Console.WriteLine("Nie ma takiego studenta w bazie.");
                }
            }
            _courseService.ChangeCourseDate(courseDto);
        }

        public void AddPerson()
        {
            var personDto = new PersonDto();
            Console.WriteLine("podaj imię");
            personDto.StudentName = Console.ReadLine();
            Console.WriteLine("podaj nazwisko");
            personDto.StudentSurname = Console.ReadLine();

            bool work1 = true;
            while (work1)
            {
                personDto.Pesel = ConsoleReadHelper.GetLong("podaj pesel");
                if (_personService.CheckIfPersonIsInDbById(personDto.Id))
                {
                    Console.WriteLine("Student już jest w bazie");
                }
                else
                    work1 = false;
            }
            personDto.Birthday = ConsoleReadHelper.GetDate("podaj datę ur");
            bool work = true;
            while (work)
            {
                Console.WriteLine("podaj płeć: F/M ");
                var sex = Console.ReadLine();
                if (sex == "M" || sex == "F")
                {
                    personDto.Sex = (PersonDto.PersonSex)Enum.Parse(typeof(PersonDto.PersonSex), sex);
                    work = false;
                }
                else
                    Console.WriteLine("Błędne dane");
            }
            _personService.AddStudent(personDto);
        }

        public void AddAttendance()
        {

            var id = ConsoleReadHelper.GetCourse();
            var courseService = new CourseService();
            var people = courseService.GetAllStudent(id);
            var attendance = new List<AttendanceDTo>();

            foreach (var personDTo in people)
            {

                var attendanceDTo = new AttendanceDTo();
                Console.WriteLine(personDTo.StudentName + " " + personDTo.StudentSurname);
                attendanceDTo.PresenceNumber = ConsoleReadHelper.GetInt("podaj liczbę obecności studenta:");
                attendanceDTo.Days = ConsoleReadHelper.GetInt("podaj liczbę dni kursu :");


                attendanceDTo.Pesel = personDTo.Pesel;
                attendance.Add(attendanceDTo);
            }
            _attendanceService.AddAttenadance(id, attendance);
        }

        public void AddHomework()
        {
            var id = ConsoleReadHelper.GetCourse();
            var maxNumberOfPoints = ConsoleReadHelper.GetInt("podaj maxymalną liczbę punktów za zadanie :");
            var courseService = new CourseService();
            var people = courseService.GetAllStudent(id);
            var homeworks = new List<HomeworkDTo>();

            foreach (var personDTo in people)
            {
                var homeworkDto = new HomeworkDTo();
                Console.WriteLine(personDTo.StudentName + " " + personDTo.StudentSurname);
                homeworkDto.NumberOfPoints = ConsoleReadHelper.GetInt("podaj liczbę punktów studenta za zadanie :");

                homeworkDto.MaxNumberOfPoints = maxNumberOfPoints;
                homeworkDto.Pesel = personDTo.Pesel;
                homeworks.Add(homeworkDto);
            }
            _homeworkService.AddHomework(id, homeworks);
        }

        public void AddCourse()
        {
            var courseDto = new CourseDTo();

            Console.WriteLine("Podaj nazwę kursu:");

            bool work1 = true;
            while (work1)
            {
                courseDto.CourseName = Console.ReadLine();
                var match = RegexCourseName(courseDto.CourseName);

                if (match == false)
                {
                    Console.WriteLine("Podaj nazwę kursu: ");
                }
                else
                    work1 = false;

            }

            Console.WriteLine("podaj dane prowadzącego");
            courseDto.CourseLeader = Console.ReadLine();
            courseDto.CourseStartDate = ConsoleReadHelper.GetDate("podaj datę rozpoczęcia kursu");
            courseDto.ThresholdPoint = ConsoleReadHelper.GetInt("podaj próg dla prac domowych");
            courseDto.Presence = ConsoleReadHelper.GetInt("podaj próg dla obecności");
            courseDto.NumberOfStudents = ConsoleReadHelper.GetInt("podaj liczbę studentów");

            List<int> listOfStudents = new List<int>();

            int count = 0;
            while (count < courseDto.NumberOfStudents)
            {
                bool work = true;
                while (work)
                {
                    int id = ConsoleReadHelper.GetInt("podaj id studenta ");
                    if (_personService.CheckIfPersonIsInDbById(id))
                    {
                        if (listOfStudents.Contains(id))
                        {
                            Console.WriteLine("Na kursie jest już osoba o tym peselu, podaj inny pesel.");
                        }
                        else
                        {
                            listOfStudents.Add((id));
                            work = false;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Nie ma takiego studenta w bazie.");
                    }
                }
                count++;
            }
            _courseService.CourseWithStudents(courseDto, listOfStudents);
        }

        private bool RegexCourseName(string tekst)
        {
            var regexIsbn = new Regex(@"^C#_[a-zA-Z_0-9]+_[A-Z]{2}$");
            var match = regexIsbn.IsMatch(tekst);
            return match;
        }
    }
}
