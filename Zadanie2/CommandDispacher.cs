﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using business.Dtos;
using business.Services;

namespace Zadanie2
{
    public class CommandDispacher
    {
        private Dictionary<string, ChooseFunctionHendler> _dictionaryWithDelegates =
            new Dictionary<string, ChooseFunctionHendler>();

        public delegate void ChooseFunctionHendler();
        public ChooseFunctionHendler ChoosedFunction;

        public Dictionary<string, ChooseFunctionHendler> TrafsferDictionary()
        {
            return _dictionaryWithDelegates;
        }
    }

}