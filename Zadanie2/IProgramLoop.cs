namespace Zadanie2
{
    public interface IProgramLoop
    {
        void PrintRaport();
        void ChangeStudentData();
        void ChangeCourseData();
        void AddPerson();
        void AddAttendance();
        void AddHomework();
        void AddCourse();
      //  event ProgramLoop.RaportPrintedEventHandler RaportPrinted;
    }
}